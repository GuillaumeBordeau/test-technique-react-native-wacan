import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';

import {Styles} from '../../styles';
import Results from '../../containers/Results';


export default function Main({navigation}) {
  return (
    <View style={Styles.container}>
        <Text>Application de test pour l'entreprise Wacan</Text>
        <Results navigation={navigation} />
    </View>
  );
}