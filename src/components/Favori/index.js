import React from 'react';
import { ScrollView, Text, View, Button } from 'react-native';
import {Styles} from '../../styles';

export default function Favori({navigation, favories, delFavorie}) {
  // console.log(favories);
  return (
    <ScrollView style={Styles.resultsView}>
        {favories.map(favorite => (
            <View key={favorite.id} style={Styles.resultCard}>
                <Text style={Styles.resultName}>{favorite.name}</Text>
                <Text>{favorite.full_name}</Text>
                <Text>Owner : {favorite.owner.login}</Text>
                <Text>description : {favorite.description}</Text>
                <Text>score : {favorite.score}</Text>
              <Button color="#C62D2D" title={`Supprimer ${favorite.name} des Favori`} onPress={() => delFavorie(favorite)} />
            </View>
        ))}
    </ScrollView>
  );
}