import React from 'react';
import { Button, Text, View, TextInput, ScrollView } from 'react-native';
import {Styles} from '../../styles';
import Result from './Result.js';


export default function Results({navigation, results, askResults, onChangeText, inputValue, setFavorites, favories}) {
  return (
    <View style={Styles.bodyContainer}>
      <TextInput
        style={Styles.textInput}
        onChangeText={onChangeText}
        placeholder= 'search...'
        value={inputValue}
        onEndEditing={askResults}
      />
      <Button title="Voir les Favoris" onPress={() => (navigation.navigate('Favori'))} />
      <ScrollView style={Styles.resultsView} scrollToOverflowEnabled>
        {results.length > 0 ? <Text>les resultats</Text> : <Text>La barre de recherche permet de faire une recherche github</Text>}
        {/* {results.length = 0 && <Text>La barre de recherche permet de faire une recherche github</Text>} */}
          {results.map(result => (
            <View key={result.id}>
              <Result result={result} />
              <Button color="#4247EF" title={`Mettre ${result.name} en Favori`} onPress={() => setFavorites(result)} />
            </View>
          ))}
      </ScrollView>
    </View>
  );
}