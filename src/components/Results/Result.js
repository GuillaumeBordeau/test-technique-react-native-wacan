import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import {Styles} from '../../styles';


export default function Result({result}) {
  // const [value, onChangeText] = React.useState();
  return (
    <View style={Styles.resultCard}>
        <Text style={Styles.resultName}>{result.name}</Text>
        <Text>{result.full_name}</Text>
        <Text>Owner : {result.owner.login}</Text>
        <Text>description : {result.description}</Text>
        <Text>score : {result.score}</Text>
    </View>
  );
}