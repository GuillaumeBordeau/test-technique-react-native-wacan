import { StyleSheet } from 'react-native';

export const Styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      // justifyContent: 'center',
      paddingTop: '20%',
      width: '100%',
    },
    bodyContainer: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      width: '100%',
    },
    textInput: {
      width: '80%',
      marginTop: '5%',
      borderColor: 'gray',
      borderWidth: 1,
      height: 30,
      borderRadius: 15,
      paddingLeft: 10,
      paddingRight: 10,
    },
    resultsView: {
      flex: 1,
    },
    resultCard: {
      width: '100%',
      borderWidth: 1,
      borderColor: 'blue',
      borderRadius: 15,
      padding: '3%',
      marginTop: '1%',
    },
    resultName: {
      fontSize: 20,
    },
  });