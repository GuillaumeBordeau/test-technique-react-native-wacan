// == Import : npm
import { connect } from 'react-redux';

// == Import : local
import Favori from '../../components/Favori';

// Action Creators
import { delFavorie } from '../../store/reducer.js';

const mapStateToProps = (state, ownProps) => ({

  favories: state.favories,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
    delFavorie: (favorie) => {
        dispatch(delFavorie(favorie));
      },
});

// Container
const FavoriContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Favori);

// == Export
export default FavoriContainer;