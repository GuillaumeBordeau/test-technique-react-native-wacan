// == Import : npm
import { connect } from 'react-redux';

// == Import : local
import Results from '../../components/Results';

// Action Creators
import { getResults, OnInputChange, setFavorites } from '../../store/reducer.js';
const mapStateToProps = (state, ownProps) => ({
  results: state.results,
  inputValue: state.value,
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  askResults: () => {
    dispatch(getResults());
  },
  onChangeText: (value) => {
    dispatch(OnInputChange(value));
  },
  setFavorites: (result) => {
    dispatch(setFavorites(result));
  },
});

// Container
const ResultsContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(Results);

// == Export
export default ResultsContainer;