// == Initial State
const initialState = {
    results: [],
    favories: [],
    inputValue: '',
  };
  
  // == Types
  export const GET_RESULTS = 'GET_RESULTS';
  const SET_RESULTS = 'SET_RESULTS';
  const CHANGE_INPUT_VALUE = 'CHANGE_INPUT_VALUE';
  const SET_FAVORITES = 'SET_FAVORITES';
  const DEL_FAVORITES = 'DEL_FAVORITES';
  
  // == Reducer
  export const reducer = (state = initialState, action = {}) => {
    switch (action.type) {
      case SET_RESULTS:
        return {
          ...state,
          results: action.results,
        };
      case CHANGE_INPUT_VALUE:
        return {
          ...state,
          inputValue: action.value,
        };
      case SET_FAVORITES:
      let exist = false;
        state.favories.forEach((favorie) => {
          if(favorie.id === action.result.id){
            exist = true;
          }
        });
        if(exist === true) {
          return {
            ...state
          }
        } else {
          state.favories.push(action.result);
          return {
            ...state,
            favories: state.favories,
          };
        }
      case DEL_FAVORITES:
        let newFav = [];
        state.favories.forEach((favorie) => {
          if(favorie.id !== action.delFav.id){
            newFav.push(favorie);
          }
        });
          return {
            ...state,
            favories: newFav,
          };
      default:
        return state;
    }
  };
  
  // == Action Creators
  export const getResults = () => ({
    type: GET_RESULTS,
  });
  export const OnInputChange = value => ({
    type: CHANGE_INPUT_VALUE,
    value,
  })
  export const setResults = results => ({
    type: SET_RESULTS,
    results,
  });
  export const setFavorites = result => ({
    type: SET_FAVORITES,
    result,
  });
  export const delFavorie = delFav => ({
    type: DEL_FAVORITES,
    delFav,
  });
  
  
  // == Selectors
  
  
  // == Export
  export default reducer;