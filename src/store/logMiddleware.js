import axios from 'axios';

import {
  GET_RESULTS,
  setResults,
} from './reducer.js';

const logMiddleware = store => next => (action) => {
    console.log('Je suis le middleware, et je laisse passer cette action: ', action);
    next(action);

    switch (action.type) {
      case GET_RESULTS:
      console.log(store.getState().inputValue);
        axios.request({
          method: 'get',
          url: `https://api.github.com/search/repositories?q=${store.getState().inputValue}`
        }).then(response => {
            store.dispatch(setResults(response.data.items));
        }).catch(err => {
          console.log(err);
        })
      break;
      default:
         next(action);
    }
  };
  
  export default logMiddleware;