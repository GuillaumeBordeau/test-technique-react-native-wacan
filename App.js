import React from 'react';
import { Provider } from 'react-redux';

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import store from './src/store';
import Main from './src/components/Main';
import Favori from './src/containers/Favori';

const MainNavigator = createStackNavigator(
  {
    Home: Main,
    Favori: Favori,
  },
  {
    initialRouteName: 'Home',
  }
);
  
const Navigator = createAppContainer(MainNavigator);

export default function App() {
  return (
    <Provider store={store}>
      <Navigator />
    </Provider>
  );
}